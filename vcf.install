<?php

/**
 * @file
 * Installation hooks for Visting Card (vcf) module.
 */

/**
 * Implements hook_install().
 */
function vcf_install() {
  // Update user form display configuration.
  $formDisplay = \Drupal::entityTypeManager()
    ->getStorage('entity_form_display')
    ->load('user.user.default');

  // Check if the field_full_name component exists.
  if (!$formDisplay->getComponent('field_full_name')) {
    // Add the 'field_full_name' component.
    $formDisplay->setComponent('field_full_name', [
      'type' => 'string_textfield',
      'weight' => 1,
      'region' => 'content',
      'settings' => [
        'size' => 60,
        'placeholder' => '',
      ],
      'third_party_settings' => [],
    ]);
    $formDisplay->save();
  }

  // Check if the field_phone_number component exists.
  if (!$formDisplay->getComponent('field_phone_number')) {
    // Add the 'field_phone_number' component.
    $formDisplay->setComponent('field_phone_number', [
      'type' => 'phone_number_default',
      'weight' => 2,
      'region' => 'content',
      'settings' => [
        'default_country' => 'US',
        'placeholder' => 'Phone number',
        'phone_size' => 15,
        'extension_size' => 5,
      ],
      'third_party_settings' => [],
    ]);
    $formDisplay->save();
  }

  // Update user view display configuration.
  $viewDisplay = \Drupal::entityTypeManager()
    ->getStorage('entity_view_display')
    ->load('user.user.default');

  // Check if the field_full_name component exists.
  if (!$viewDisplay->getComponent('field_full_name')) {
    // Add the 'field_full_name' component.
    $viewDisplay->setComponent('field_full_name', [
      'type' => 'string',
      'label' => 'hidden',
      'settings' => [
        'link_to_entity' => FALSE,
      ],
      'third_party_settings' => [],
      'weight' => 1,
      'region' => 'content',
    ]);
    $viewDisplay->save();
  }

  // Check if the field_phone_number component exists.
  if (!$viewDisplay->getComponent('field_phone_number')) {
    // Add the 'field_phone_number' component.
    $viewDisplay->setComponent('field_phone_number', [
      'type' => 'phone_number_international',
      'label' => 'hidden',
      'settings' => [
        'as_link' => FALSE,
      ],
      'third_party_settings' => [],
      'weight' => 2,
      'region' => 'content',
    ]);
    $viewDisplay->save();
  }
}
