# Visting Card (vcf)

Visting Card (vcf) intends to provide a functionaity to generate
virtual contact file cards for users, offering a seamless way to
download the user contact details.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/vcf).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/search/vcf).

## Table of contents

- Requirements
- Recommended modules
- Installation
- Configuration
- Similar Projects
- Maintainers

## Requirements

This module is independent of all contrib modules and operates
within Drupal core. No additional dependencies are needed for
basic functionality and only requires "User" module from the core
to be installed.

## Recommended modules

There are no dependencies for this module and all the features
would be extended in this module itself. Thus, no additional
modules or libraries are required for this standalone module.


## Installation

Install as you would normally install a contributed Drupal module.
For further information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).
Upon installation, the module creates Full name and Phone number
field in user profiles, then you need to visit people's page,
there you will get an option to download their VCF (Virtual Contact
File) card under the operations dropdown. You can customize cards by
editing user data. Ensure all required fields are populated to avoid
errors during card creation.

## Configuration

No Configuration is required.

## Similar Projects
While there are existing modules like vCard (Drupal 7 and inactive
after 2011 with no further releases) and Business Card (inactive
since 2006 and no releases), our solution is tailored for Drupal 9
and 10, ensuring compatibility with the latest versions and providing
modern features.

## Maintainers

Current maintainers:

- [Dhruvesh Tripathi (dhruveshdtripathi)](https://www.drupal.org/u/dhruveshdtripathi)
- [Urvashi Vora (urvashi_vora)](https://www.drupal.org/u/urvashi_vora)

Supporting organizations:

- [atlas-softweb-pvt-ltd](https://www.drupal.org/atlas-softweb-pvt-ltd) Created and sponsored this module and supported in the module development.
