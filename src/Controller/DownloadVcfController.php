<?php

namespace Drupal\vcf\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\Response;

/**
 * Controller for handling the Download VCF functionality.
 */
class DownloadVcfController extends ControllerBase {

  /**
   * Controller callback for generating and downloading VCF card.
   *
   * @param int $uid
   *   The user ID.
   *
   * @return array|\Symfony\Component\HttpFoundation\Response
   *   The render array or the VCF card download response.
   */
  public function downloadVcf($uid) {
    // Load the user entity.
    $user = \Drupal\user\Entity\User::load($uid);

    // Get user data.
    $full_name = $user->get('field_full_name')->value;
    $phone_number = $user->get('field_phone_number')->value;
    $email = $user->getEmail();

    // Check if required fields are filled out.
    if (empty($full_name) || empty($phone_number) || empty($email)) {
      $missingFields = [];
      if (empty($full_name)) {
        $missingFields[] = 'Name';
      }
      if (empty($phone_number)) {
        $missingFields[] = 'Phone Number';
      }
      if (empty($email)) {
        $missingFields[] = 'Email';
      }

      if (!empty($missingFields)) {
        // Display Drupal error message.
        $message = $this->t('VCF could not be downloaded, the following fields are required: @fields', ['@fields' => implode(', ', $missingFields)]);
        $this->messenger()->addError($message);

        // Redirect back to the user's profile page.
        return $this->redirect('<current>');
      }
    } else {
      // Generate VCF content.
      $vcfContent = $this->vcfContent($full_name, $phone_number, $email);
      $username = str_replace(' ', '_', $full_name);
      // Set response headers for download.
      $response = new Response($vcfContent);
      $response->headers->set('Content-Type', 'text/vcard');
      $response->headers->set('Content-Disposition', 'attachment; filename="' . $username . '.vcf"');
      return $response;
    }
  }

  /**
   * Generate Virtual contact file (VCF) content based on user data.
   *
   * @param string $full_name
   *   The full name of the user.
   * @param string $phone_number
   *   The phone number of the user.
   * @param string $email
   *   The email address of the user.
   *
   * @return string
   *   The Virtual contact file (VCF) content.
   */
  protected function vcfContent($full_name, $phone_number, $email) {
    $vcfContent = "BEGIN:VCARD\r\n";
    $vcfContent .= "VERSION:3.0\r\n";
    $vcfContent .= "FN:" . $full_name . "\r\n";
    $vcfContent .= "TEL:" . $phone_number . "\r\n";
    $vcfContent .= "EMAIL:" . $email . "\r\n";
    $vcfContent .= "END:VCARD";
    return $vcfContent;
  }
}
