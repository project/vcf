<?php

/**
 * @file
 * Hook implementation for the Visting Card (vcf) module.
 */

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;

/**
 * Implements hook_help().
 */
function vcf_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.vcf':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Generate and manage professional Visiting (VCF) Cards for users. This module seamlessly integrates with user profiles, allowing users to download their contact details. For more details, visit <a href="https://www.drupal.org/project/vcf">module</a> page') . '</p>';
      $output .= '<h3>' . t('Uses') . '</h3>';
      $output .= '<p>' . t('There are existing modules like vCard (Drupal 7 and inactive after 2011 with no further releases) and Business Card (inactive since 2006 and no releases), our solution is tailored for Drupal 9 and 10, ensuring compatibility with the latest versions and providing modern features. But the Visiting Card (VCF) module would provide you an instant functionality to download VCF card of users enrolled in the site. And it can be used in attorney websites, doctor websites, tutor websites and any such website where we need to download the user contact information as vcf card') . '</p>';
      return $output;
  }
}

/**
 * Implements hook_entity_operation_alter().
 */
function vcf_entity_operation_alter(array &$operations, EntityInterface $entity) {
  if ($entity->getEntityTypeId() == 'user') {
    $operations['download_vcf'] = [
      'title' => t('Download VCF'),
      'weight' => 150,
      'url' => Url::fromRoute('vcf.download_vcf', ['uid' => $entity->id()]),
      'attributes' => ['class' => ['button']],
    ];
  }
}
